DESTDIR=/

all: install

install:
	mkdir -p $(DESTDIR)/usr/share/backgrounds/tearch/ || true
	mkdir -p $(DESTDIR)/usr/share/tearch-background-properties/ || true

	cp -prfv backgrounds/* $(DESTDIR)/usr/share/backgrounds/tearch/
	cp -prfv properties/* $(DESTDIR)/usr/share/tearch-background-properties/